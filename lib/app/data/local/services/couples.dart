import 'package:becouple/core/base/base_storage.dart';

class CoupleStorage extends BaseStorage{

  Future<void> setCouple(Map<String,dynamic> data) async{
    await storage.write('couple', data);
  }

  Map<String, dynamic> getCouple(){
    var coupleData = storage.read<Map<String,dynamic>>('couple');
    return coupleData;
  }

}