import 'package:intl/intl.dart';

class Couple{
  String yourImage;
  String theirImage;
  String yourName;
  String theirName;
  String userID;
  DateTime date;

  Couple();

  Couple.fromJson(Map<String,dynamic> map){
    DateFormat dateFormat = DateFormat("dd-MM-yyyy");
    this.yourImage = map['yourImage'];
    this.theirImage = map['theirImage'];
    this.theirName = map['theirName'];
    this.yourName = map['yourName'];
    this.userID = map['userID'];
    this.date = dateFormat.parse(map['date']);
  }

  Map<String,dynamic> toJson(){
    DateFormat dateFormat = DateFormat('dd-MM-yyyy');
    return {
      'yourImage':this.yourImage,
      'theirImage':this.theirImage,
      'yourName':this.yourName,
      'theirName':this.theirName,
      'userID':this.userID,
      'date':dateFormat.format(this.date),
    };
  }
}