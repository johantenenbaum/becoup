import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
part 'memory_data.g.dart';

@HiveType(typeId: 1)
class MemoryData extends Equatable{
  @HiveField(1)
  String id;
  @HiveField(2)
  String title;
  @HiveField(3)
  String imageURL;
  @HiveField(4)
  String desc;
  @HiveField(5)
  String location;
  @HiveField(6)
  String tag;
  @HiveField(7)
  DateTime date;

  MemoryData(
      {this.id,
      this.title,
      this.imageURL,
      this.desc,
      this.location,
      this.tag,
      this.date});

  MemoryData.fromJson(Map<String, dynamic> json) {
    DateFormat dateFormat = DateFormat('dd-MM-yyyy');
    id = json['id'];
    title = json['title'];
    imageURL = json['imageURL'];
    desc = json['desc'];
    location = json['location'];
    tag = json['tag'];
    date = dateFormat.parse(json['date']);
  }

  toJson() => {
    ''
  };

  @override
  List<Object> get props => [id];
}
