// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'memory_data.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class MemoryDataAdapter extends TypeAdapter<MemoryData> {
  @override
  final int typeId = 1;

  @override
  MemoryData read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return MemoryData(
      id: fields[1] as String,
      title: fields[2] as String,
      imageURL: fields[3] as String,
      desc: fields[4] as String,
      location: fields[5] as String,
      tag: fields[6] as String,
      date: fields[7] as DateTime,
    );
  }

  @override
  void write(BinaryWriter writer, MemoryData obj) {
    writer
      ..writeByte(7)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.title)
      ..writeByte(3)
      ..write(obj.imageURL)
      ..writeByte(4)
      ..write(obj.desc)
      ..writeByte(5)
      ..write(obj.location)
      ..writeByte(6)
      ..write(obj.tag)
      ..writeByte(7)
      ..write(obj.date);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is MemoryDataAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
