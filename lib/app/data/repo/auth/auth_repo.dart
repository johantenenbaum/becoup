import 'dart:async';
import 'dart:io';
import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/core/managers/firebase_manager/authentication.dart';
import 'package:becouple/core/managers/firebase_manager/firestore.dart';
import 'package:becouple/core/managers/firebase_manager/storage.dart';
import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository{
  FireBaseAuthentication _auth = FireBaseAuthentication();
  FirebaseFireStore _fireStore = FirebaseFireStore();
  FirebaseStorageManager _storageManager = FirebaseStorageManager();
  CouplesRepository _couplesRepository = CouplesRepository();

  Future<void> signInWithGoogle()async{
    await _auth.signInWithGoogle();
  }

  Future<void> signInWithFacebook() async{
    await _auth.signInWithFacebook();
  }

  User getCurrentUser(){
    return _auth.currentUser();
  }

  Stream<User> userStatus(){
    return _auth.userStatus();
  }
  

  Future<void> signOut()async{
    await _auth.signOut();
  }

  Future<String> uploadFile({File file,bool isYour})async{
    var userID = _auth.currentUser().uid;
    var url = await _storageManager.uploadImage(userID: userID,file: file,isYour: isYour);
    return url;
  }

  Future<void> addUser({String yourName, String theirName, String date, String yourImage, String theirImage})async{
    var userID = _auth.currentUser().uid;
    await _fireStore.addUser(userID: userID,yourName: yourName,theirName: theirName,date: date,yourImage: yourImage,theirImage: theirImage);
    await _couplesRepository.setCoupleToLocal({
      'yourName': yourName,
      'theirName': theirName,
      'yourImage': yourImage,
      'theirImage': theirImage,
      'date': date,
      'userID':userID
    });
  }

  Future<bool> userIsAvailable(String userID)async{
    var status = await _fireStore.userIsAvailable(userID: userID);
    return status;
  } 

  
}