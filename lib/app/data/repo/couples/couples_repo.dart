import 'package:becouple/app/data/local/services/couples.dart';
import 'package:becouple/app/data/remote/model/couples.dart';
import 'package:becouple/core/managers/firebase_manager/authentication.dart';
import 'package:becouple/core/managers/firebase_manager/firestore.dart';
import 'package:connectivity/connectivity.dart';

class CouplesRepository {
  FirebaseFireStore _fireStore = FirebaseFireStore();
  FireBaseAuthentication _auth = FireBaseAuthentication();
  CoupleStorage coupleStorage = CoupleStorage();

  Future<Couple> getCouples()async{
    Couple couple;
    var result = await Connectivity().checkConnectivity();

    if(result == ConnectivityResult.wifi || result == ConnectivityResult.mobile){
      var id = _auth.currentUser().uid;
      couple = await _fireStore.fetchCouples(id);
    }else{
      couple = getCoupleFromLocal();
    }
    return couple;
  }

  Couple getCoupleFromLocal(){
    var data = coupleStorage.getCouple();
    var couple = Couple.fromJson(data);
    return couple;
  }

  Future<void> setCoupleToLocal(Map<String, dynamic> data)async{
    await coupleStorage.setCouple(data);
  }

}