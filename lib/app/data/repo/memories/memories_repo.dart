import 'dart:io';
import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/core/managers/firebase_manager/firestore.dart';
import 'package:becouple/core/managers/firebase_manager/storage.dart';
import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';
import 'package:uuid/uuid.dart';
import 'package:hive_flutter/hive_flutter.dart';

class MemoriesRepository{

  FirebaseFireStore _fireStore = FirebaseFireStore();
  FirebaseStorageManager _storageManager = FirebaseStorageManager();
  AuthRepository _authRepository = AuthRepository();

  Future<String> uploadMemoryImage(File file)async{
    var userID = _authRepository.getCurrentUser().uid;
    var url = await _storageManager.uploadMemories(userID: userID,file: file);
    return url;
  }

  Future<void> addMemory({String imageURL, String title, String description, String date, String location, String tag})async{
    var uuid = Uuid();
    var id = uuid.v1();
    var userID = _authRepository.getCurrentUser().uid;
    await _fireStore.addMemory(userID,uuid: id,title: title,imageURL: imageURL,description: description,date: date,location: location,tag: tag);
    
    var memory = MemoryData.fromJson({
      'id' : id,
      'imageURL' : imageURL,
      'title' : title,
      'desc' : description,
      'date':date,
      'location' : location,
      'tag':tag
    });

    await _saveToLocal(memory);
  }

  Future<void> fetchMemories()async{
    var userID = _authRepository.getCurrentUser().uid;
    var list = await _fireStore.fetchMemories(userID);
    var memoriesList = list.map((e) => MemoryData.fromJson(e)).toList();
    await Hive.box<MemoryData>('memories').clear();
    for(var item in memoriesList){
      await _saveToLocal(item);
    }
  }

  List<MemoryData> getMemories(){
    var box = Hive.box<MemoryData>('memories');
    return box.values.toList();
  }

  Future<void> updateMemoryToLocal({MemoryData oldValue,MemoryData newValue})async{
    var box = Hive.box<MemoryData>('memories');
    var list = box.values.toList();
    list.removeWhere((element) => element == oldValue);
    list.add(newValue);
    await box.clear();
    await box.addAll(list);
  }

  Future<void> deleteMemory(MemoryData data)async{
    var userID = _authRepository.getCurrentUser().uid;
    var box = Hive.box<MemoryData>('memories');
    var list = box.values.toList();
    list.removeWhere((element) => element == data);
    await box.clear();
    await box.addAll(list);
    await _fireStore.deleteMemory(userID, data.id);
  }

  Future<void> _saveToLocal(MemoryData value)async{
    var box = Hive.box<MemoryData>('memories');
    await box.add(value);
  }

  ValueListenable<Box<MemoryData>> listanbleMemoriesData(){
    var box = Hive.box<MemoryData>('memories');
    return box.listenable();
  }

  Future<void> updateMemory(String documentID,Map<String,dynamic> data)async{
    var userID = _authRepository.getCurrentUser().uid;
    await _fireStore.updateMemory(userID, documentID,data);
  }


}