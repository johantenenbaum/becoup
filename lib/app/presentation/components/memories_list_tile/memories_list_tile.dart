import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class MemoriesListTile extends StatelessWidget {
  final String title;
  final String location;
  final String tag;
  final DateTime date;

  MemoriesListTile({
    @required this.title,
    @required this.location,
    @required this.tag,
    @required this.date,
  });

  DateFormat _dateFormat = DateFormat('dd-MM-yyyy');

  
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, bottom: 8, top: 8),
      height: Get.height * 0.090,
      width: Get.width * 0.85,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(color: Color(0xFFE8E8E8), spreadRadius: 3, blurRadius: 5)
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: 16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 8,
              ),
              Expanded(
                child: Text(
                  title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(
                      Icons.location_on,
                      size: 16,
                      color: Color(0xFFFF3A3A),
                    ),
                    Text(
                      location,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Icon(
                      Icons.tag,
                      size: 16,
                      color: Color(0xFFFF3A3A),
                    ),
                    Text(
                      tag,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Spacer(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                _dateFormat.format(date),
                style: TextStyle(fontWeight: FontWeight.w400),
              ),
              Text(
                "in "+DateTime.now().difference(date).inDays.toString() + " days",
                style: TextStyle(color: Color(0xFFFF3A3A)),
              ),
            ],
          ),
          SizedBox(
            width: 16,
          ),
        ],
      ),
    );
  }
}
