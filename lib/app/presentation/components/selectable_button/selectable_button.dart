import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SelectableButton extends StatelessWidget {
  final String buttonText;
  final String leadingText;
  final VoidCallback onTap;

  const SelectableButton({
    @required this.leadingText,
    @required this.onTap,
    @required this.buttonText,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 55,
      width: Get.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(36), color: Color(0xFFE8E8E8)),
      child: Row(
        children: [
          SizedBox(
            width: 16,
          ),
          Text(
            leadingText,
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
          ),
          Spacer(),
          GestureDetector(
            onTap: onTap,
            child: Container(
              padding: EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 8),
              height: 35,
              decoration: BoxDecoration(
                  color: Color(0xFFFF3A3A),
                  borderRadius: BorderRadius.circular(8)),
              child: Center(
                  child: Text(
                buttonText,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 12),
              )),
            ),
          ),
          SizedBox(
            width: 16,
          ),
        ],
      ),
    );
  }
}