import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FacebookButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 65,
      width: Get.width * 0.8,
      decoration: BoxDecoration(
        color: Color(0xFF3B5998),
        boxShadow: [
          BoxShadow(color: Colors.blueGrey,blurRadius: 2)
        ],
        borderRadius: BorderRadius.circular(8)
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 16),
            child: Image.asset("assets/login/facebook.png"),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: Text("Sign in with Facebook",style: TextStyle(color: Color(0xFFFFFFFF),fontSize: 18),),
          )
        ],
      ),
    );
  }
}