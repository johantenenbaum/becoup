import 'package:flutter/material.dart';

class TextFieldX extends StatelessWidget {
  final void Function(String) onChanged;
  final String hintText;
  String labelText;
  final TextEditingController controller;

  TextFieldX({@required this.controller,@required this.onChanged,@required this.hintText,@required this.labelText});

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      onChanged: onChanged,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(color: Color(0xFFBBBBBB)),
        filled: true,
        fillColor: Color(0xFFE8E8E8),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(36),
            borderSide: BorderSide(color: Colors.transparent)
        ),
        disabledBorder:OutlineInputBorder(
            borderRadius: BorderRadius.circular(36),
            borderSide: BorderSide(color: Colors.transparent)
        ),
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(36),
            borderSide: BorderSide(color: Colors.transparent)
        ),
      ),
    );
  }
}
