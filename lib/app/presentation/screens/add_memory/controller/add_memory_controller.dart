import 'dart:io';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';

class AddMemoryController extends BaseController {
  ImagePicker picker;
  var memoryImage = File("").obs;
  var memorImageIsSelected = false.obs;

  var date = DateTime.now();
  var dateText = "".obs;
  var dateIsSelected = false.obs;


  TextEditingController titleController = TextEditingController();
  TextEditingController desctiptionController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController tagController = TextEditingController();

  MemoriesRepository _memoriesRepository = MemoriesRepository();


  @override
  void close() {
    print("Closed");
  }

  @override
  void init() {
    picker = ImagePicker();
  }


  Future getImage() async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      memoryImage.value = File(pickedFile.path);
      await _cropImage(memoryImage.value.path);
      memorImageIsSelected.value = true;
      print(memoryImage.value.path);
    } else {
      print('No image selected.');
    }
  }

  Future<void> uploadingToFirebase() async {
    if(dateIsSelected.value == false || memorImageIsSelected.value == false || titleController.text.length <=0){
      Get.defaultDialog(title: "Hata",content: Text("Zorunlu alanları boş bırakamazsınız."));
    }else{
      Get.dialog(Center(child: CircularProgressIndicator(),));

      var url = await _memoriesRepository.uploadMemoryImage(memoryImage.value);
      await _memoriesRepository.addMemory(
        title: titleController.text,
        imageURL: url,
        date: dateText.value,
        description: desctiptionController.text,
        tag: tagController.text,
        location: locationController.text
      );

      MemoriesController ctrl = Get.find();
      HomeController homeController = Get.find();

      ctrl.loadMemories();
      homeController.loadMemories();
      Get.back();
      Get.back();
    }
  }

  Future<Null> _cropImage(String path) async {
    File croppedFile = await ImageCropper.cropImage(
      cropStyle: CropStyle.circle,
        sourcePath: path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
              ]
            : [
                CropAspectRatioPreset.square,
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));

    if (croppedFile != null) {
      memoryImage.value = croppedFile;
    }
  }


}
