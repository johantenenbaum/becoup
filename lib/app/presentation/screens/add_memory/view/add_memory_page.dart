import 'package:becouple/app/presentation/components/selectable_button/selectable_button.dart';
import 'package:becouple/app/presentation/components/textfieldX/textfieldX.dart';
import 'package:becouple/app/presentation/screens/add_memory/controller/add_memory_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class AddMemoryPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddMemoryController>(
      init: AddMemoryController(),
      builder: (controller) => SafeArea(
        child: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 16),
                  child: Text(
                    "Add new memory.",
                    style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Obx(()=>SelectableButton(
                          leadingText: controller.memorImageIsSelected.value ? "Selected" :"Memory Image",
                          buttonText: "CHOOSE",
                          onTap: () {
                            controller.getImage();
                          },
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      TextFieldX(
                          controller: controller.titleController,
                          onChanged: null,
                          hintText: "Title",
                          labelText: "null"),
                      SizedBox(
                        height: 16,
                      ),
                      textArea(controller),
                      SizedBox(
                        height: 16,
                      ),
                      Obx(
                        () => SelectableButton(
                          leadingText: controller.dateIsSelected.value
                              ? controller.dateText.value
                              : "Select Date",
                          buttonText: "SELECT",
                          onTap: () async {
                            var date = await showDatePicker(
                                context: context,
                                initialDate: DateTime.now(),
                                firstDate: DateTime(2000),
                                lastDate: DateTime.now());
                            if (date != null) {
                              controller.dateIsSelected.value = true;
                              controller.date = date;
                              controller.dateText.value =
                                  DateFormat('dd-MM-yyyy').format(date);
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      TextFieldX(
                          controller: controller.locationController,
                          onChanged: null,
                          hintText: "Location (Optional)",
                          labelText: "null"),
                      SizedBox(
                        height: 16,
                      ),
                      TextField(
                        controller: controller.tagController,
                        decoration: InputDecoration(
                          hintText: "Add a tag",
                          hintStyle: TextStyle(color: Color(0xFFBBBBBB)),
                          filled: true,
                          prefixIcon: Icon(Icons.tag),
                          fillColor: Color(0xFFE8E8E8),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(36),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(36),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(36),
                              borderSide:
                                  BorderSide(color: Colors.transparent)),
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.back(result: true);
                            },
                            child: Container(
                              padding: EdgeInsets.all(8),
                              height: Get.height * 0.05,
                              width: Get.width * 0.4,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: Colors.black),
                              child: Center(
                                child: Text(
                                  "cancel",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              controller.uploadingToFirebase();
                            },
                            child: Container(
                              height: Get.height * 0.05,
                              width: Get.width * 0.4,
                              padding: EdgeInsets.all(8),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                  color: Color(0xFFFF3A3A)),
                              child: Center(
                                child: Text(
                                  "save memory",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container textArea(AddMemoryController controller) {
    return Container(
      child: TextField(
        controller: controller.desctiptionController,
        minLines: 5,
        maxLines: 15,
        autocorrect: false,
        decoration: InputDecoration(
          hintStyle: TextStyle(color: Color(0xFFBBBBBB)),
          hintText: '  Description (Optional)',
          filled: true,
          fillColor: Color(0xFFE8E8E8),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(36),
              borderSide: BorderSide(color: Colors.transparent)),
          disabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(36),
              borderSide: BorderSide(color: Colors.transparent)),
          enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(36),
              borderSide: BorderSide(color: Colors.transparent)),
        ),
      ),
    );
  }
}
