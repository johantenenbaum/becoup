import 'dart:io';

import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class EditReleationShipController extends BaseController{

  var isEdit = false.obs;
  AuthRepository _authRepository = AuthRepository();
  CouplesRepository _memoriesRepository = CouplesRepository();

  //THIRD SCENE
  ImagePicker picker;
  var yourImage = File("").obs;
  var theirImage = File("").obs;
  var yourImageIsSelected = false.obs;
  var theirImageIsSelected = false.obs;
  var yourImageNetwork = "".obs;
  var theirImageNetwork = "".obs;

  //FIRST SCENE
  var yourName = "".obs;
  var theirName = "".obs;
  TextEditingController yourNameController = TextEditingController();
  TextEditingController theirNameController = TextEditingController();

  //SECOND SCENE
  var date = DateTime.now();
  var dateText = "".obs;
  var dateIsSelected = false.obs;

  PageController pageController = PageController();
  var currentPage = 0.obs;

  var errorMessage = "".obs;


  @override
  void close() {
    
  }

  @override
  void init() {
    picker = ImagePicker();
  } 

  Future getImage(int index) async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (index == 0) {
      if (pickedFile != null) {
        var croppedFile = await _cropImage(pickedFile.path);
        yourImage.value = croppedFile ?? pickedFile;
        yourImageIsSelected.value = true;
        print(yourImage.value.path);
      } else {
        print('No image selected.');
      }
    } else {
      if (pickedFile != null) {
        var croppedFile = await _cropImage(pickedFile.path);
        theirImage.value = croppedFile ?? pickedFile;
        theirImageIsSelected.value = true;
        print(theirImage.value.path);
      } else {
        print('No image selected.');
      }
    }
  }

  Future<void> uploadingToFirebase()async{
    Get.dialog(Center(child: CircularProgressIndicator(),));
    var youImageURL = yourImageIsSelected.value ? await _authRepository.uploadFile(file: yourImage.value,isYour: true) : yourImageNetwork.value;
    var theirImageURL = theirImageIsSelected.value ? await _authRepository.uploadFile(file: theirImage.value,isYour: false) : theirImageNetwork.value;
    await _authRepository.addUser(
      yourName: yourNameController.text,
      theirName: theirNameController.text,
      yourImage:  youImageURL,
      theirImage: theirImageURL,
      date: dateText.value);
    Get.back();
    HomeController _controller = Get.find();
    var remoteData = await _memoriesRepository.getCouples();
    await _memoriesRepository.setCoupleToLocal(remoteData.toJson());
    _controller.loadCouple();
    Get.back(); 
  }

  Future<File> _cropImage(String path) async {
    File croppedFile = await ImageCropper.cropImage(
      cropStyle: CropStyle.circle,
        sourcePath: path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));

    if (croppedFile != null) {
      return croppedFile;
    }else{
      return null;
    }
  }

}