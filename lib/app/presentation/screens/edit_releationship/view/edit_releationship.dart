import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/app/presentation/screens/edit_releationship/controller/edit_releationship_controller.dart';
import 'package:becouple/app/presentation/screens/edit_releationship/view/first_setup.dart';
import 'package:becouple/app/presentation/screens/edit_releationship/view/second_setup.dart';
import 'package:becouple/app/presentation/screens/edit_releationship/view/third_setup.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';


class EditReleationShipPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: GetBuilder<EditReleationShipController>(
      init: EditReleationShipController(),
      builder: (controller) {
        initMemory(controller);
        return Scaffold(
          body: Stack(
        children: [
          buildPageView(controller),
          pageIndicator(controller),
          buildNextButton(controller),
          buildBackButton(controller),
        ],
      ));
      },
    ));
  }

  initMemory(EditReleationShipController controller)async{
    CouplesRepository repo = Get.find();
    var couple = repo.getCoupleFromLocal();
    controller.yourNameController.text = couple.yourName;
    controller.theirNameController.text = couple.theirName;
    controller.dateIsSelected.value = true;
    controller.dateText.value = DateFormat('dd-MM-yyyy').format(couple.date);
    controller.yourImageNetwork.value = couple.yourImage;
    controller.theirImageNetwork.value = couple.theirImage;
  }

  Positioned buildPageView(EditReleationShipController controller) {
    return Positioned.fill(
      child: PageView(
        onPageChanged: (index) => controller.currentPage.value = index,
        physics: NeverScrollableScrollPhysics(),
        controller: controller.pageController,
        children: [
          FirstSetup(controller),
          SecondSetup(controller),
          ThirdSetup(controller)
        ],
      ),
    );
  }

  Positioned pageIndicator(EditReleationShipController controller) {
    return Positioned.fill(
        bottom: 20,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: SmoothPageIndicator(
            controller: controller.pageController, // PageController
            count: 3,
            effect: WormEffect(activeDotColor: Color(0xFFFF3A3A)),
          ),
        ));
  }

  Positioned buildNextButton(EditReleationShipController controller) {
    return Positioned.fill(
        bottom: 15,
        right: 15,
        child: Align(
          alignment: Alignment.bottomRight,
          child: InkWell(
            child: Obx(
              () => Container(
                width: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      controller.currentPage.value == 2 ? "Finish" : "Next",
                      style: TextStyle(fontSize: 18),
                    ),
                    Icon(
                      Icons.chevron_right,
                      size: 36,
                      color: Color(0xFFFF3A3A),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              if (controller.currentPage.value == 0) {
                if (controller.yourNameController.text.length == 0 ||
                    controller.theirNameController.text.length == 0) {
                  controller.errorMessage.value =
                      "Error! You should enter names to continue";
                } else {
                  controller.pageController.nextPage(
                      duration: Duration(milliseconds: 750),
                      curve: Curves.easeInOutSine);
                }
              } else if (controller.currentPage.value == 1) {
                if (controller.dateIsSelected.value) {
                  controller.pageController.nextPage(
                      duration: Duration(milliseconds: 750),
                      curve: Curves.easeInOutSine);
                }
              } else {
                  controller.uploadingToFirebase();
                  //controller.pageController.nextPage(duration: Duration(milliseconds: 750),curve: Curves.easeInOutSine);    
              }
            },
          ),
        ));
  }

  Obx buildBackButton(EditReleationShipController controller) {
    return Obx(
      () => Positioned.fill(
          bottom: 15,
          left: 15,
          child: controller.currentPage.value == 0
              ? Container()
              : Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    width: 80,
                    child: InkWell(
                      child: Row(
                        children: [
                          Icon(
                            Icons.chevron_left,
                            size: 36,
                            color: Color(0xFFFF3A3A),
                          ),
                          Text(
                            "Back",
                            style: TextStyle(fontSize: 18),
                          ),
                        ],
                      ),
                      onTap: () {
                        controller.pageController.animateToPage(
                            controller.pageController.page.toInt() - 1,
                            duration: Duration(milliseconds: 750),
                            curve: Curves.easeInOutSine);
                      },
                    ),
                  ),
                )),
    );
  }
}
