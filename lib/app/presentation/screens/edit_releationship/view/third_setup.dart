import 'package:becouple/app/presentation/screens/edit_releationship/controller/edit_releationship_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ThirdSetup extends StatelessWidget {

  final EditReleationShipController controller;
  ThirdSetup(this.controller);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFFFFFFF),
      child: Column(
        children: [
          SizedBox(
            height: 36,
          ),
          Column(
            children: [
              Text(
                "Your Photo",
                style: TextStyle(fontSize: 24),
              ),
              SizedBox(
                height: 8,
              ),
              GestureDetector(
                onTap: () {
                  controller.getImage(0);
                },
                child: Obx(
                  () => Container(
                    height: 180,
                    width: 180,
                    decoration: BoxDecoration(
                        color: Color(0xFFE8E8E8),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: controller.yourImageIsSelected.value ? BoxFit.fill : BoxFit.none,
                          image: controller.yourImageIsSelected.value
                              ? FileImage(controller.yourImage.value)
                              : NetworkImage(controller.yourImageNetwork.value),
                        )),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 26,
          ),
          Column(
            children: [
              Text(
                "Their Photo",
                style: TextStyle(fontSize: 24),
              ),
              SizedBox(
                height: 8,
              ),
              GestureDetector(
                onTap: () {
                  controller.getImage(1);
                },
                child: Obx(
                  () => Container(
                    height: 180,
                    width: 180,
                    decoration: BoxDecoration(
                        color: Color(0xFFE8E8E8),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: controller.theirImageIsSelected.value ? BoxFit.fill : BoxFit.none,
                          image: controller.theirImageIsSelected.value
                              ? FileImage(controller.theirImage.value)
                              : NetworkImage(controller.theirImageNetwork.value),
                        )),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
