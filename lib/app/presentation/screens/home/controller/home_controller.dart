import 'dart:io';
import 'dart:typed_data';

import 'package:becouple/app/data/remote/model/couples.dart';
import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:screenshot/screenshot.dart';
import 'package:time_machine/time_machine.dart';


class HomeController extends BaseController {

  CouplesRepository couplesRepository = Get.find();
  AuthRepository authRepository = Get.find();
  PageController  pageController;
  var currentIndex = 0.obs;
  MemoriesRepository memoriesRepository = MemoriesRepository();
  var memoriesList = List<MemoryData>().obs;

  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;


  //COUPLE HEADER
  var coupleIsLoaded = false.obs;
  var couple = Couple().obs;

  //COUPLE FOR DATE
  var year = 0.obs;
  var month = 0.obs;
  var day = 0.obs;
  var totalDay = 0.obs;

  //POPUP MENU ITEMS
  var popUpMenuItems = ["Settings","Edit Relationship","Share Screen","Rate Us"];


  //Share Screen
  var isShareTapped = false.obs;


  @override
  void close() {
  }

  @override
  void init() {
  loadCouple();
  loadMemories();
  pageController = PageController();
  }


  loadCouple(){
    couple.value = couplesRepository.getCoupleFromLocal();
    var date = couple.value.date;

    LocalDate now = LocalDate.today();
    LocalDate b = LocalDate.dateTime(date);
    Period diff = now.periodSince(b);

    year.value = diff.years;
    month.value = diff.months;
    day.value = diff.days;

    totalDay.value = DateTime.now().difference(couple.value.date).inDays;

    coupleIsLoaded.value = true;
  }

  loadMemories(){
    var list = memoriesRepository.getMemories();
    list.sort((a,b){
      return b.date.compareTo(a.date);
    });
    memoriesList.assignAll(list.take(3));
    MemoriesController memo = Get.find();
    memo.memoriesList = memoriesList;
  }

  shareScreenShot()async{
    _imageFile = null;
    isShareTapped.value = true;
    screenshotController.capture(delay: Duration(milliseconds: 10), pixelRatio: 2.0).then((File image) async {
        _imageFile = image;
      Uint8List pngBytes = _imageFile.readAsBytesSync();
      print("File Saved to Gallery");
      isShareTapped.value = false;
      await Share.file('Image', 'screenshot.png', pngBytes, 'image/png');
    }).catchError((onError) {
      print(onError);
    });
  }

}