import 'package:becouple/app/presentation/screens/add_memory/view/add_memory_page.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/premium/view/premium_page.dart';
import 'package:becouple/app/presentation/screens/show_memory/view/show_memory_page.dart';
import 'package:becouple/core/managers/admob_manager/admob_manager.dart';
import 'package:becouple/core/managers/purchase_helper/purchase_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CounterPage extends StatelessWidget {
  final defaultTextStyleW300 =
      TextStyle(fontSize: 25, fontWeight: FontWeight.w300);
  final defaultTextStyleW500 =
      TextStyle(fontSize: 25, fontWeight: FontWeight.w500);
  final redTextStyle = TextStyle(
      color: Color(0xFFFF3A3A), fontSize: 25, fontWeight: FontWeight.w300);
  final redTextStyleBold = TextStyle(
      color: Color(0xFFFF3A3A), fontSize: 25, fontWeight: FontWeight.bold);

  final HomeController controller;
  CounterPage(this.controller, {Key key}) : super(key: key);

  final list = [1, 2, 3];

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildeCouples(),
        Obx(()=>Expanded(
              flex: 3,
              child: Container(
                  width: Get.width,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        "Couple For",
                        style: defaultTextStyleW300,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Text(
                                "${controller.year} ",
                                style: redTextStyle,
                              ),
                              Text(
                                " Year",
                                style: defaultTextStyleW500,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "${controller.month} ",
                                style: redTextStyle,
                              ),
                              Text(
                                "Month",
                                style: defaultTextStyleW500,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "${controller.day} ",
                                style: redTextStyle,
                              ),
                              Text(
                                "Days",
                                style: defaultTextStyleW500,
                              ),
                            ],
                          ),
                        ],
                      ),
                      Text("-or-"),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "${controller.totalDay}",
                            style: redTextStyleBold,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            "days",
                            style: defaultTextStyleW300,
                          ),
                        ],
                      ),
                      Text("-upcoming-")
                    ],
                  ),
              )),
        ),
        Expanded(
            flex: 4,
            child: Container(
                width: Get.width,
                child: Obx(
                  () => Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: controller.memoriesList
                        .map(
                          (e) => InkWell(
                            onTap: () {
                              Get.to(ShowMemoryPage(memory:e));
                            },
                            child: Container(
                              margin: EdgeInsets.only(left: 16, right: 16),
                              height: Get.height * 0.090,
                              width: Get.width * 0.85,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xFFE8E8E8),
                                        spreadRadius: 3,
                                        blurRadius: 5)
                                  ]),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Text(
                                    e.title,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  Spacer(),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        DateFormat('dd-MM-yyyy').format(e.date),
                                        style: TextStyle(
                                            fontWeight: FontWeight.w400),
                                      ),
                                      Text(
                                        "in " +
                                            DateTime.now()
                                                .difference(e.date)
                                                .inDays
                                                .toString() +
                                            " days",
                                        style:
                                            TextStyle(color: Color(0xFFFF3A3A)),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                        .toList(),
                  ),
                ))),
        Obx(() {
          if (controller.memoriesList.length < 3) {
            return GestureDetector(
              onTap: () {
                if (PurchaseHelper.shared.isPremium) {
                  Get.to(AddMemoryPage());
                } else {
                  if (controller.memoriesList.length > 1) {
                    Get.to(PremiumPage());
                  } else {
                    AdmobManager.showInterstatial();
                    Get.to(AddMemoryPage());
                  }
                }
              },
              child: controller.isShareTapped.value ? Container() : Container(
                margin: EdgeInsets.only(left: 16, bottom: 16, right: 16),
                height: Get.height * 0.07,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    color: Color(0xFFFF3A3A)),
                child: Center(
                  child: Text(
                    "Add New Memory",
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                ),
              ),
            );
          } else {
            return Container();
          }
        }),
        FutureBuilder(
            future: Future.delayed(Duration(seconds: 1), () {
              return true;
            }),
            builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
              if (snapshot.hasData) {
                if(PurchaseHelper.shared.isPremium){
                  return Container();
                }else{
                  return AdmobManager.banner();
                }
              } else {
                return Container();
              }
            })
      ],
    );
  }

  Widget buildeCouples() {
    return Obx(()=> Expanded(
          flex: 3,
          child: Container(
            width: Get.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                        backgroundColor: Colors.transparent,
                        minRadius: 25,
                        maxRadius: 50,
                        backgroundImage: CachedNetworkImageProvider(
                            controller.couple.value.yourImage)),
                    SizedBox(
                      height: 8,
                    ),
                    Text(controller.couple.value.yourName)
                  ],
                ),
                Column(
                  children: [
                    Spacer(),
                    Image.asset(
                      "assets/home/heart.png",
                      height: 100,
                      width: 100,
                    )
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircleAvatar(
                        backgroundColor: Colors.transparent,
                        minRadius: 25,
                        maxRadius: 50,
                        backgroundImage: CachedNetworkImageProvider(
                            controller.couple.value.theirImage)),
                    SizedBox(
                      height: 8,
                    ),
                    Text(controller.couple.value.theirName)
                  ],
                )
              ],
            ),
          )),
    );
  }
}
