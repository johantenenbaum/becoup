import 'package:becouple/app/presentation/screens/edit_releationship/view/edit_releationship.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/memories/view/memories_page.dart';
import 'package:becouple/app/presentation/screens/settings/settings_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:becouple/app/presentation/screens/home/view/counter.dart';
import 'package:screenshot/screenshot.dart';
import 'package:xuxu_ui/xuxu_ui.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GetBuilder<HomeController>(
        init: HomeController(),
        builder: (controller) => Screenshot(
          controller: controller.screenshotController,
          child: Scaffold(
            body: Container(
                width: Get.width,
                height: Get.height,
                color: Color(0xFFFFFFFF),
                child: Column(
                  children: [buildHeader(controller), buildView(controller)],
                )),
          ),
        ),
      ),
    );
  }

  Widget buildView(HomeController controller) {
    return Obx(() {
      if (!controller.coupleIsLoaded.value) {
        return Expanded(
          child: Container(
            width: Get.width,
            height: Get.height,
            child: Center(
              child: CircularProgressIndicator(
                backgroundColor: Colors.red,
              ),
            ),
          ),
        );
      } else {
        return Expanded(
          child: PageView(
              onPageChanged: (page) => controller.currentIndex.value = page,
              controller: controller.pageController,
              children: [CounterPage(controller), MemoriesPage()]),
        );
      }
    });
  }

  Widget buildHeader(HomeController controller) {
    return Obx(
      () => Container(
        width: Get.width,
        margin: EdgeInsets.only(top: 8),
        child: Row(
          children: [
            Spacer(),
            PopupMenuButton(
                color: Colors.transparent,
                icon: Icon(Icons.more_vert, color: Colors.transparent),
                itemBuilder: (ctx) {
                  return [];
                }),
            Text(
              "counter",
              style: TextStyle(
                  color: controller.currentIndex.value == 0
                      ? Color(0xFFFF3A3A)
                      : Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.w300),
            ).onTap(() {
              controller.pageController.animateToPage(0,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOut);
            }),
            SizedBox(
              width: 16,
            ),
            Text(
              "memories",
              style: TextStyle(
                  color: controller.currentIndex.value == 1
                      ? Color(0xFFFF3A3A)
                      : Colors.black,
                  fontSize: 25,
                  fontWeight: FontWeight.w300),
            ).onTap(() {
              controller.pageController.animateToPage(1,
                  duration: Duration(milliseconds: 500),
                  curve: Curves.easeInOut);
            }),
            Spacer(),
            PopupMenuButton(onSelected: (selectedItem) {
              if (selectedItem == "Settings") {
                Get.to(SettingsPage());
              } else if (selectedItem == "Edit Relationship") {
                Get.to(EditReleationShipPage());
              } else if (selectedItem == "Share Screen") {
                print("Tıklandı");
                controller.shareScreenShot();
              }
            }, itemBuilder: (ctx) {
              return controller.popUpMenuItems
                  .map((e) => PopupMenuItem(
                        child: Text(
                          e,
                          textAlign: TextAlign.center,
                        ),
                        value: e,
                      ))
                  .toList();
            }),
            SizedBox(
              width: 8,
            ),
          ],
        ),
      ),
    );
  }
}
