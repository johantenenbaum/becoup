import 'dart:async';
import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/app/presentation/screens/home/view/home.dart';
import 'package:becouple/app/presentation/screens/setup/view/setup_page.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends BaseController{

  AuthRepository _authRepository = Get.find();
  CouplesRepository _couplesRepository = Get.put(CouplesRepository());
  MemoriesRepository _memoriesRepository = Get.put(MemoriesRepository());
  Stream<User> user;
  
  @override
  void close() {
    user = null;
  }

  @override
  void init() {
    print("XD");
    userControl();
  }

  signInWithGoogle(){
    _authRepository.signInWithGoogle();
  }

  signInWithFacebook() async{
    await _authRepository.signInWithFacebook();
  }

  userControl(){
    user = _authRepository.userStatus();
    user.listen((event) async{
      if(event != null){
        var userIsAvailable = await _authRepository.userIsAvailable(event.uid);
        if(userIsAvailable){
          var couple = await _couplesRepository.getCouples();
          await _couplesRepository.setCoupleToLocal(couple.toJson());
          await _memoriesRepository.fetchMemories();
        }
        userIsAvailable ? transitionPage(HomePage()) : transitionPage(SetupPage());
      }
    });
  }

  transitionPage(Widget page){
      Get.off(page,transition: Transition.fadeIn,duration: Duration(milliseconds: 500),curve: Curves.easeIn);
  }

}