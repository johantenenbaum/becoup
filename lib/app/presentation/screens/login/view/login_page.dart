import 'package:becouple/app/presentation/components/social_button/facebook_button.dart';
import 'package:becouple/app/presentation/components/social_button/google_button.dart';
import 'package:becouple/app/presentation/screens/login/controller/login_controller.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends StatelessWidget {

  final LoginController controller = Get.put(LoginController());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            buildHeaderImage(),
            buildSocialButtons(),
            buildFooter(),
          ],
        ),
      ),
    );
  }

  Expanded buildFooter() {
    return Expanded(
        flex: 1,
        child: Text.rich(
          TextSpan(
            children: <TextSpan>[
              TextSpan(text: 'by signing up you are agreeing our \n'),
              TextSpan(
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      print("Tıklandı");
                    },
                  text: 'terms of services',
                  style: TextStyle(
                      decoration: TextDecoration.underline, color: Colors.red)),
              TextSpan(text: ' and '),
              TextSpan(
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      print("Tıklandı");
                    },
                  text: 'privacy policies',
                  style: TextStyle(
                      decoration: TextDecoration.underline, color: Colors.red)),
            ],
          ),
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 17.0),
        ));
  }

  Expanded buildSocialButtons() {
    return Expanded(
        flex: 4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(onTap: (){
              controller.signInWithGoogle();
            },child: GoogleButton()),
            SizedBox(
              height: 16,
            ),
            GestureDetector(onTap: (){
              controller.signInWithFacebook();
            },child: FacebookButton())
          ],
        ));
  }
  
  Expanded buildHeaderImage() {
    return Expanded(
      flex: 5,
      child: Image.asset(
        "assets/login/login_image.png",
        fit: BoxFit.contain,
      ),
    );
  }
}
