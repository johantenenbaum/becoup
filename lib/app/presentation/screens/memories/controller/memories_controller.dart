import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:get/get.dart';

class MemoriesController extends BaseController {

  MemoriesRepository memoriesRepository = MemoriesRepository();
  var memoriesList = List<MemoryData>().obs;

  @override
  void close() {
  }

  @override
  void init() {
    loadMemories();
  }

  loadMemories(){
    var dataList = memoriesRepository.getMemories();
    dataList.sort((a,b){
      return b.date.compareTo(a.date);
    });
    memoriesList.assignAll(dataList);
  }

}