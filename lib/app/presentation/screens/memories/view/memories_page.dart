import 'package:becouple/app/presentation/components/memories_list_tile/memories_list_tile.dart';
import 'package:becouple/app/presentation/screens/add_memory/view/add_memory_page.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:becouple/app/presentation/screens/premium/view/premium_page.dart';
import 'package:becouple/app/presentation/screens/show_memory/view/show_memory_page.dart';
import 'package:becouple/core/managers/admob_manager/admob_manager.dart';
import 'package:becouple/core/managers/purchase_helper/purchase_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MemoriesPage extends GetView<MemoriesController> {
  MemoriesPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            flex: 2,
            child: Center(
                child: Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Text(
                "Add more couple memories.",
                style: TextStyle(fontSize: 36),
              ),
            )),
          ),
          Expanded(
            flex: 8,
            child: Column(
              children: [
                Obx(() {
                  if (controller.memoriesList.length > 0) {
                    return buildList();
                  } else {
                    return emptyView();
                  }
                }),
                Spacer(),
                GestureDetector(
                  onTap: () {
                    if (PurchaseHelper.shared.isPremium) {
                      Get.to(AddMemoryPage());
                    } else {
                      if (controller.memoriesList.length > 1) {
                        Get.to(PremiumPage());
                      } else {
                        AdmobManager.showInterstatial();
                        Get.to(AddMemoryPage());
                      }
                    }
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 16, bottom: 16, right: 16),
                    height: Get.height * 0.07,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Color(0xFFFF3A3A)),
                    child: Center(
                      child: Text(
                        "Add New Memory",
                        style: TextStyle(color: Colors.white, fontSize: 22),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  emptyView() {
    return Expanded(
      flex: 8,
      child: Column(
        children: [
          Expanded(
            flex: 8,
            child: Image.asset("assets/memories/empty.png"),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8, right: 8),
            child: Text(
              "There is no any memory, yet.Add a new one.",
              style: TextStyle(fontSize: 26),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  Widget buildList() {
    return Obx(
      () => Expanded(
          flex: 10,
          child: ListView.builder(
              itemCount: controller.memoriesList.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Get.to(ShowMemoryPage(
                      memory: controller.memoriesList[index],
                    ));
                  },
                  child: MemoriesListTile(
                    title: controller.memoriesList[index].title,
                    location: controller.memoriesList[index].location,
                    date: controller.memoriesList[index].date,
                    tag: controller.memoriesList[index].tag,
                  ),
                );
              })),
    );
  }
}
