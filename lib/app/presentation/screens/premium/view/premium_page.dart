import 'package:becouple/core/managers/purchase_helper/purchase_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:xuxu_ui/xuxu_ui.dart';
import 'package:purchases_flutter/purchases_flutter.dart';


class PremiumPage extends StatefulWidget {

  @override
  _PremiumPageState createState() => _PremiumPageState();
}

class _PremiumPageState extends State<PremiumPage> {
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFFFFFFF),
        body: SingleChildScrollView(
                  child: XuVStack([
            XuBox(child: Icon(Icons.arrow_back,size: 28,)).alignment(Alignment.topLeft).marginOnly(left: 16,top: 8).create().onTap(() {
              Navigator.pop(context);
            }),
            XuText("Add more couple memories.").align(TextAlign.center).fontSize(36).fontWeight(FontWeight.bold).create(),
            XuZStack([

              XuBox(
                child: XuVStack([

                  XuBox().create(),
                  XuText("Premium features:").fontWeight(FontWeight.bold).fontSize(25).create(),
                  XuText("No banner ads!").fontWeight(FontWeight.w400).fontSize(25).create(),
                  XuText("No interstitial ads!").fontWeight(FontWeight.w400).fontSize(25).create(),
                  XuText("Unlimited memories!").fontWeight(FontWeight.w400).fontSize(25).create(),
                  SpaceV(8),
                  XuText("Monthly \$2.99 \n Start 3-day trial").align(TextAlign.center).fontWeight(FontWeight.bold).fontSize(25).create(),
                  
                  XuBox(
                    child: XuText("Go Premium").color(Colors.white).fontSize(20).fontWeight(FontWeight.bold).create().center()
                  ).size(width: 190,height: 60).decoration(XuBoxDecoration().color(Color(0xFFFF3A3A)).radiusAll(radius: 36)).create().onTap(() async{

                    var purchaseStatus = await PurchaseHelper.shared.purchase("1month_ps");

                    if(purchaseStatus){
                      Get.snackbar("Ödeme İşlemi", "Başarıyla Satın Alındı");
                      await Future.delayed(Duration(milliseconds: 1500),(){
                        Get.back();
                        Get.back();
                      });
                    }
                    else{
                      Get.snackbar("Ödeme İşlemi", "Satın alınamadı");
                    }  
                    
                  }),
                  SpaceV(0),
                  XuText("3 days free trial, then \$2.99/month. Cancel anytime.").align(TextAlign.center).fontSize(20).fontWeight(FontWeight.w400).create().paddingSymmetric(horizontal: 8),

                  ExpansionTile(title: XuText("Term of Use").create().center(),leading: Icon(Icons.import_contacts),children: [
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          text: 'Subscriptions will automatically renew unless canceled within 24-hours before the end of the current period.You can cancel anytime with your iTunes account settings.For more information, see our',
                          style: TextStyle(color: Colors.black,fontSize: 12),
                          children: <TextSpan>[
                            TextSpan(text: ' Terms of Use ', style: TextStyle(fontWeight: FontWeight.bold,decoration: TextDecoration.underline)),
                            TextSpan(text: 'and'),
                            TextSpan(text: ' Privacy Policy ', style: TextStyle(fontWeight: FontWeight.bold,decoration: TextDecoration.underline)),
                            TextSpan(text: "If you already purchased you can"),
                            TextSpan(text: " restore.\n",style: TextStyle(fontWeight: FontWeight.bold,decoration: TextDecoration.underline))
                          ],
                        ),
                      ),
                  ],).paddingSymmetric(horizontal: 8),

                ]).space(8).create()
              ).decoration(XuBoxDecoration().radiusAll().color(Color(0xFFFFF4F4))).size(width: Get.width * 0.8).create(),

              Image.asset("assets/premium/heart.png",height: 80,width: 80,).positioned(right: -40,top: -40)

            ]).clip(Clip.none).create()
          ]).space(20).create(),
        )
      ),
    );
  }
}