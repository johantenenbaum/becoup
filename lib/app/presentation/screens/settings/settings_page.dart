import 'dart:io';

import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/login/view/login_page.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:rate_my_app/rate_my_app.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  HomeController homeController = Get.find();
  var status = true;
  final titleList = [
    "Facebook community",
    "Instagram community",
    "terms of service",
    "privacy policy",
    "rate us",
    "requests/feedbacks"
  ];

  RateMyApp rateMyApp = RateMyApp(
    preferencesPrefix: 'rateMyApp_',
    minDays: 7,
    minLaunches: 10,
    remindDays: 7,
    remindLaunches: 10,
    googlePlayIdentifier: 'com.rocksolid.becoupleapp',
    appStoreIdentifier: '1491556149',
  );

  final Uri _emailLaunchUri = Uri(
      scheme: 'mailto',
      path: 'contact@becoupleapp.com',
      queryParameters: {'subject': 'Feedback BeCouple'});

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    super.initState();
    rateMyApp.init();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFFFFFFFF),
        body: Column(
          children: [
            SizedBox(
              height: 8,
            ),
            Container(
              child: Row(
                children: [
                  BackButton(
                    color: Colors.black,
                  ),
                  Spacer(),
                  Text(
                    "Settings",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 36,
                        fontWeight: FontWeight.bold),
                  ),
                  Spacer(),
                  BackButton(
                    color: Colors.transparent,
                  ),
                ],
              ),
            ),
            ListTile(
              title: Text(
                " Notifications",
                style: TextStyle(fontSize: 26, fontWeight: FontWeight.w300),
              ),
              trailing: FlutterSwitch(
                activeColor: Color(0xFFFF3A3A),
                width: 60.0,
                height: 25.0,
                value: status,
                borderRadius: 25.0,
                onToggle: (val) {
                  setState(() {
                    status = val;
                  });
                },
              ),
            ),
            Expanded(
              flex: 9,
              child: ListView.builder(
                  itemCount: titleList.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        handleItems(index);
                      },
                      child: Container(
                        margin: EdgeInsets.only(
                            left: 16, right: 16, bottom: 8, top: 8),
                        height: Get.height * 0.070,
                        width: Get.width * 0.85,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFE8E8E8),
                                  spreadRadius: 3,
                                  blurRadius: 5)
                            ]),
                        child: ListTile(
                            title: Text(
                              titleList[index],
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w300),
                            ),
                            trailing: index == 0
                                ? Image.asset("assets/settings/facebook.png")
                                : index == 1
                                    ? Image.asset("assets/settings/insta.png")
                                    : Icon(Icons.chevron_right)),
                      ),
                    );
                  }),
            ),
            GestureDetector(
              onTap: () async {
                //showStarRate();
                await Hive.box<MemoryData>('memories').clear();
                await homeController.authRepository.signOut();
                MemoriesController memories = Get.find();
                memories.memoriesList.clear();
                Get.offAll(LoginPage());
              },
              child: Container(
                margin: EdgeInsets.only(left: 16, bottom: 8, right: 16),
                width: Get.width * 0.60,
                height: Get.height * 0.055,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(0xFFFF3A3A)),
                child: Center(
                  child: Text(
                    "Log out",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 22,
                        fontWeight: FontWeight.w300),
                  ),
                ),
              ),
            ),
            Text(
              "version 1.0.0",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
            ),
            SizedBox(
              height: 8,
            )
          ],
        ),
      ),
    );
  }

  showStarRate() {
    rateMyApp.showStarRateDialog(
      context,
      title: 'Rate this app', // The dialog title.
      message:
          'You like this app ? Then take a little bit of your time to leave a rating :',
      actionsBuilder: (context, stars) {
        return [
          FlatButton(
            child: Text('VOTE'),
            onPressed: () async {
              var value = stars.toInt();
              if (value > 3) {
                print('Yönlendir');
                _launchURL(
                    "https://play.google.com/store/apps/details?id=com.droemobile.xprofile&hl=tr");
              } else {
                _launchURL(_emailLaunchUri.toString());
              }
              await rateMyApp.callEvent(RateMyAppEventType.rateButtonPressed);
              Navigator.pop<RateMyAppDialogButton>(
                  context, RateMyAppDialogButton.rate);
            },
          ),
        ];
      },
      ignoreNativeDialog: Platform
          .isAndroid, // Set to false if you want to show the Apple's native app rating dialog on iOS or Google's native app rating dialog (depends on the current Platform).
      dialogStyle: DialogStyle(
        // Custom dialog styles.
        titleAlign: TextAlign.center,
        messageAlign: TextAlign.center,
        messagePadding: EdgeInsets.only(bottom: 20),
      ),
      starRatingOptions: StarRatingOptions(), // Custom star bar rating options.
      onDismissed: () => rateMyApp.callEvent(RateMyAppEventType
          .laterButtonPressed), // Called when the user dismissed the dialog (either by taping outside or by pressing the "back" button).
    );
  }

  void handleItems(int index) {
    switch (index) {
      case 0:
        _launchURL("https://www.facebook.com/becoupleapp");
        break;
      case 1:
        _launchURL("https://www.instagram.com/becoupleapp/");
        break;
      case 2:
        _launchURL("https://www.facebook.com/becoupleapp");
        break;
      case 3:
        _launchURL("https://www.facebook.com/becoupleapp");
        break;
      case 4:
        showStarRate();
        break;
      case 5:
        _launchURL(_emailLaunchUri.toString());
        break;
      default:
    }
  }
}
