

import 'dart:io';
import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/presentation/screens/home/view/home.dart';
import 'package:becouple/app/presentation/screens/setup/view/first_setup.dart';
import 'package:becouple/app/presentation/screens/setup/view/second_setup.dart';
import 'package:becouple/app/presentation/screens/setup/view/third_setup.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class SetupController extends GetxController{

  var isEdit = false.obs;
  AuthRepository _authRepository = AuthRepository();

  //THIRD SCENE
  ImagePicker picker;
  var yourImage = File("").obs;
  var theirImage = File("").obs;
  var yourImageIsSelected = false.obs;
  var theirImageIsSelected = false.obs;

  //FIRST SCENE
  var yourName = "".obs;
  var theirName = "".obs;
  TextEditingController yourNameController = TextEditingController();
  TextEditingController theirNameController = TextEditingController();

  //SECOND SCENE
  var date = DateTime.now();
  var dateText = "".obs;
  var dateIsSelected = false.obs;

  PageController pageController = PageController();
  var currentPage = 0.obs;

  var pageList = [FirstSetup(key: PageStorageKey(0),),SecondSetup(key: PageStorageKey(1)),ThirdSetup(key: PageStorageKey(2))];
  var errorMessage = "".obs;

  @override
  void onInit() {
    picker = ImagePicker();
    loadIsEdit();
    super.onInit();
  }

  loadIsEdit(){
    if(isEdit.value){
      print("EVET EDİT");
    }else{
    }
  }

  Future getImage(int index) async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (index == 0) {
      if (pickedFile != null) {
        var croppedFile = await _cropImage(pickedFile.path);
        yourImage.value = croppedFile ?? pickedFile;
        yourImageIsSelected.value = true;
        print(yourImage.value.path);
      } else {
        print('No image selected.');
      }
    } else {
      if (pickedFile != null) {
        var croppedFile = await _cropImage(pickedFile.path);
        theirImage.value = croppedFile ?? pickedFile;
        theirImageIsSelected.value = true;
        print(theirImage.value.path);
      } else {
        print('No image selected.');
      }
    }
  }

  Future<void> uploadingToFirebase()async{
    Get.dialog(Center(child: CircularProgressIndicator(),));
    var youImageURL = await _authRepository.uploadFile(file: yourImage.value,isYour: true);
    var theirImageURL = await _authRepository.uploadFile(file: theirImage.value,isYour: false);
    await _authRepository.addUser(
      yourName: yourNameController.text,
      theirName: theirNameController.text,
      yourImage: youImageURL,
      theirImage: theirImageURL,
      date: dateText.value);
    Get.back();
    Get.offAll(HomePage());
  }

  Future<File> _cropImage(String path) async {
    File croppedFile = await ImageCropper.cropImage(
      cropStyle: CropStyle.circle,
        sourcePath: path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
              ]
            : [
                CropAspectRatioPreset.square,
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));

    if (croppedFile != null) {
      return croppedFile;
    }else{
      return null;
    }
  } 

}