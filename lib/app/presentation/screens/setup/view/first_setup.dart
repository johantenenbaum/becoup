import 'package:becouple/app/presentation/components/textfieldX/textfieldX.dart';
import 'package:becouple/app/presentation/screens/setup/controller/demo.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FirstSetup extends GetView<SetupController> {
  final Key key;
  FirstSetup({this.key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFFFFFFF),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Start being a couple",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 36,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFieldX(
                controller: controller.yourNameController,
                labelText: controller.theirName.value,
                onChanged: (str) {
                  controller.yourName.value = str;
                },
                hintText: "Your Name",
              )),
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFieldX(
                controller: controller.theirNameController,
                labelText: controller.theirName.value,
                onChanged: (str) {
                  controller.theirName.value = str;
                },
                hintText: "Their Name",
              )),
          Obx(() {
            if (controller.yourName.value.length == 0 ||
                controller.theirName.value.length == 0) {
              return Padding(
                padding: const EdgeInsets.only(left: 16, right: 16, top: 8),
                child: Container(
                  height: 60,
                  decoration: BoxDecoration(
                      color: Color(0xFFFFFFFF),
                      borderRadius: BorderRadius.circular(36),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xFF000000).withOpacity(0.20),
                            blurRadius: 4)
                      ]),
                  child: Center(
                    child: Text("You should enter names to continue"),
                  ),
                ),
              );
            } else {
              return Container();
            }
          })
        ],
      ),
    );
  }
}
