import 'package:becouple/app/presentation/screens/setup/controller/demo.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class SecondSetup extends GetView<SetupController> {

  final Key key;
  SecondSetup({this.key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFFFFFFFF),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              "When did you become a couple",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 36,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          ),
          GestureDetector(
            onTap: () async{
             var date = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2000),
                  lastDate: DateTime.now());
                  
                  if(date != null){
                    controller.dateIsSelected.value = true;
                    controller.date = date;
                    controller.dateText.value = DateFormat('dd-MM-yyyy').format(date);
                    print(DateTime(date.year,date.month,date.day));
                  }
            },
            child: Container(
              width: Get.width * 0.8,
              height: 55,
              decoration: BoxDecoration(
                  color: Color(0xFFFF3a3a),
                  borderRadius: BorderRadius.circular(12)),
              child: Center(
                child: Obx((){
                  var text = "";
                  if(controller.dateIsSelected.value){
                    text = controller.dateText.value;
                  }else{
                    text = "Select Date";
                  }
                  return Text(
                    text,
                    style: TextStyle(color: Colors.white, fontSize: 19),
                  );
                }),
              ),
            ),
          )
        ],
      ),
    );
  }
}
