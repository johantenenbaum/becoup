import 'package:becouple/app/presentation/screens/setup/controller/demo.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class SetupPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GetBuilder<SetupController>(
        init: SetupController(),
      builder: (controller) {
        return Scaffold(
            body: Stack(
          children: [
            buildPageView(controller),
            pageIndicator(controller),
            buildNextButton(controller),
            buildBackButton(controller),
          ],
        ));
      },
    ));
  }

  Positioned buildPageView(SetupController controller) {
    return Positioned.fill(
      child: PageView.builder(
          onPageChanged: (index) => controller.currentPage.value = index,
          physics: NeverScrollableScrollPhysics(),
          itemCount: controller.pageList.length,
          controller: controller.pageController,
          itemBuilder: (context, index) {
            return controller.pageList[index];
          }),
    );
  }

  Positioned pageIndicator(SetupController controller) {
    return Positioned.fill(
        bottom: 20,
        child: Align(
          alignment: Alignment.bottomCenter,
          child: SmoothPageIndicator(
            controller: controller.pageController, // PageController
            count: 3,
            effect: WormEffect(activeDotColor: Color(0xFFFF3A3A)),
          ),
        ));
  }

  Positioned buildNextButton(SetupController controller) {
    return Positioned.fill(
        bottom: 15,
        right: 15,
        child: Align(
          alignment: Alignment.bottomRight,
          child: InkWell(
            child: Obx(
              () => Container(
                width: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      controller.currentPage.value == 2 ? "Finish" : "Next",
                      style: TextStyle(fontSize: 18),
                    ),
                    Icon(
                      Icons.chevron_right,
                      size: 36,
                      color: Color(0xFFFF3A3A),
                    ),
                  ],
                ),
              ),
            ),
            onTap: () {
              if (controller.currentPage.value == 0) {
                if (controller.yourNameController.text.length == 0 ||
                    controller.theirNameController.text.length == 0) {
                  controller.errorMessage.value =
                      "Error! You should enter names to continue";
                } else {
                  controller.pageController.nextPage(
                      duration: Duration(milliseconds: 750),
                      curve: Curves.easeInOutSine);
                }
              } else if (controller.currentPage.value == 1) {
                if (controller.dateIsSelected.value) {
                  controller.pageController.nextPage(
                      duration: Duration(milliseconds: 750),
                      curve: Curves.easeInOutSine);
                }
              } else {
                if (controller.yourImageIsSelected.value &&
                    controller.theirImageIsSelected.value) {
                  controller.uploadingToFirebase();
                  //controller.pageController.nextPage(duration: Duration(milliseconds: 750),curve: Curves.easeInOutSine);
                }
              }
            },
          ),
        ));
  }

  Obx buildBackButton(SetupController controller) {
    return Obx(
      () => Positioned.fill(
          bottom: 15,
          left: 15,
          child: controller.currentPage.value == 0
              ? Container()
              : Align(
                  alignment: Alignment.bottomLeft,
                  child: Container(
                    width: 80,
                    child: InkWell(
                      child: Row(
                        children: [
                          Icon(
                            Icons.chevron_left,
                            size: 36,
                            color: Color(0xFFFF3A3A),
                          ),
                          Text(
                            "Back",
                            style: TextStyle(fontSize: 18),
                          ),
                        ],
                      ),
                      onTap: () {
                        controller.pageController.animateToPage(
                            controller.pageController.page.toInt() - 1,
                            duration: Duration(milliseconds: 750),
                            curve: Curves.easeInOutSine);
                      },
                    ),
                  ),
                )),
    );
  }
}
