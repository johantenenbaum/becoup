import 'dart:io';
import 'dart:typed_data';

import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:get/get.dart';
import 'package:screenshot/screenshot.dart';

class ShowMemoryController extends BaseController{

  MemoriesRepository _memoriesRepository = Get.find();
  ScreenshotController screenshotController = ScreenshotController();
  MemoryData memoryData;
  File _imageFile;

  ShowMemoryController(this.memoryData);
  
  @override
  void close() {
  }

  @override
  void init() {
  }

  Future<void> deleteMemory()async{
    await _memoriesRepository.deleteMemory(memoryData);
    MemoriesController ctrl = Get.find();
    HomeController homeController = Get.find();

    ctrl.loadMemories();
    homeController.loadMemories();
    Get.back();
  }

  shareScreenShot()async{
    _imageFile = null;
    screenshotController.capture(delay: Duration(milliseconds: 10), pixelRatio: 2.0).then((File image) async {
        _imageFile = image;
      Uint8List pngBytes = _imageFile.readAsBytesSync();
      print("File Saved to Gallery");
      await Share.file('Image', 'screenshot.png', pngBytes, 'image/png');
    }).catchError((onError) {
      print(onError);
    });
  }
}