import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/presentation/screens/show_memory/controller/show_memory_controller.dart';
import 'package:becouple/app/presentation/screens/update_memory/view/update_memory.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:screenshot/screenshot.dart';

class ShowMemoryPage extends StatelessWidget {
  final MemoryData memory;

  ShowMemoryPage({@required this.memory});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: ShowMemoryController(memory),
      builder: (controller) => SafeArea(
        child: Screenshot(
          controller: controller.screenshotController,
                child: Scaffold(
            extendBodyBehindAppBar: true,
            backgroundColor: Color(0xFFFFFFFF),
            body: Column(
              children: [
                SizedBox(height: 8,),
                Container(
                  child: Row(
                    children: [
                      BackButton(
                        color: Colors.black,
                      ),
                      Spacer(),
                      Text(
                        "It's your history",
                        style: TextStyle(color: Colors.black, fontSize: 36),
                      ),
                      Spacer(),
                      PopupMenuButton(
                        onSelected: (item) async{
                          if(item == 0){
                            var result = await Get.to(UpdateMemoryPage(memory));
                            if(!result){
                              Get.back();
                            }
                          }else{
                            //print("Tıklandı");
                            controller.deleteMemory();
                          }
                        },
                          icon: Image.asset('assets/memories/edit.png'),
                          itemBuilder: (ctx) {
                            return [
                              PopupMenuItem(child: Text("Edit",textAlign: TextAlign.center,),value: 0,),
                              PopupMenuItem(child: Text("Delete",textAlign: TextAlign.center,),value:1),
                            ];
                          }),
                    ],
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 75,
                  backgroundImage: CachedNetworkImageProvider(memory.imageURL)

                ),
                Text(
                  memory.title,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 29,
                      fontWeight: FontWeight.bold),
                ).paddingOnly(top: 8, bottom: 8),
                Text(
                  DateFormat('dd-MM-yyyy').format(memory.date),
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w300,
                      color: Colors.black),
                ),
                Text(
                  "25 days ago",
                  style: TextStyle(
                      color: Color(0xFFFF3A3A),
                      fontSize: 16,
                      fontWeight: FontWeight.w300),
                ),
                SizedBox(
                  height: 16,
                ),
                Text(memory.desc ?? "",
                        style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.w400,
                            color: Color(0xFF151515)),
                        textAlign: TextAlign.center)
                    .paddingOnly(left: 8, right: 8),
                SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.location_on,
                      color: Color(0xFFFF3A3A),
                    ),
                    Text(
                      memory.location ?? "",
                      style: TextStyle(fontSize: 21, fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.tag,
                      color: Color(0xFFFF3A3A),
                    ),
                    Text(
                      memory.tag,
                      style: TextStyle(fontSize: 21, fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
                Spacer(),
                GestureDetector(
                  onTap: (){
                    controller.shareScreenShot();
                  },
                  child: Container(
                    height: Get.height * 0.06,
                    width: Get.width * 0.8,
                    padding: EdgeInsets.all(8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Color(0xFFFF3A3A)),
                    child: Center(
                      child: Text(
                        "share memory",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
