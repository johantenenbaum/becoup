import 'dart:async';
import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/presentation/screens/home/view/home.dart';
import 'package:becouple/app/presentation/screens/login/view/login_page.dart';
import 'package:becouple/app/presentation/screens/setup/view/setup_page.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashController extends BaseController {

  AuthRepository _loginRepository = Get.find();


  StreamSubscription<User> user;

  @override
  void close() {
    print("Splash Kapandı");
    user.cancel();
  }

  @override
  void init() {
    currentUserControl();
  }

  transitionPage(Widget page){
    Future.delayed(Duration(milliseconds: 1300),(){
      Get.off(page,transition: Transition.fadeIn,duration: Duration(milliseconds: 500),curve: Curves.easeIn);
    });
  }

  currentUserControl() async{
    var currentUser = _loginRepository.getCurrentUser();
    if(currentUser == null){
      transitionPage(LoginPage());
    }else{
      var status = await _loginRepository.userIsAvailable(currentUser.uid);
      if(status) 
      transitionPage(HomePage()); 
      else 
      transitionPage(SetupPage());
    }
  }
  
}