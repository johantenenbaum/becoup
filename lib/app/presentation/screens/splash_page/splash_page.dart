import 'package:becouple/app/presentation/screens/splash_page/controller/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashPage extends GetView<SplashController> {
  
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Stack(
        children: [
          Positioned(
              right: 0,
              child: Image.asset(
                "assets/splash/flowers.png",
                width: 365,
                height: 300,
              )),
          Positioned.fill(
              child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        "assets/splash/icon.png",
                        width: 108,
                        height: 206,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Image.asset(
                        "assets/splash/logo.png",
                        width: 290,
                        height: 75,
                      ),
                    ],
                  )))
        ],
      ),
    ));
  }
}
