import 'dart:io';
import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:becouple/core/base/base_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class UpdateMemoryController extends BaseController {
  ImagePicker picker;
  var memoryImage = File("").obs;
  var memorImageIsSelected = false.obs;

  var date = DateTime.now();
  var dateText = "".obs;
  var dateIsSelected = false.obs;


  TextEditingController titleController = TextEditingController();
  TextEditingController desctiptionController = TextEditingController();
  TextEditingController locationController = TextEditingController();
  TextEditingController tagController = TextEditingController();

  MemoriesRepository _memoriesRepository = MemoriesRepository();

  MemoryData memory;

  UpdateMemoryController(this.memory){
    titleController.text = memory.title;
    desctiptionController.text = memory.desc;
    locationController.text = memory.location;
    tagController.text = memory.tag;
    dateIsSelected.value = true;
    dateText.value = DateFormat('dd-MM-yyyy').format(memory.date);
  }


  @override
  void close() {
    
  }

  @override
  void init() {
    picker = ImagePicker();
  }



  Future getImage() async {
    var pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      memoryImage.value = File(pickedFile.path);
      await _cropImage(memoryImage.value.path);
      memorImageIsSelected.value = true;
      print(memoryImage.value.path);
    } else {
      print('No image selected.');
    }
  }

  Future<void> uploadingToFirebase() async {
    if(dateIsSelected.value == false || titleController.text.length <=0){
      Get.defaultDialog(title: "Hata",content: Text("Zorunlu alanları boş bırakamazsınız."));
    }else{
      Get.dialog(Center(child: CircularProgressIndicator(),));
      await _memoriesRepository.updateMemory(memory.id,{
        'title': titleController.text,
        'date': dateText.value,
        'desc': desctiptionController.text,
        'tag': tagController.text,
        'location': locationController.text
      });
      var newValue = MemoryData.fromJson({
        'title': titleController.text,
        'id': memory.id,
        'imageURL' : memory.imageURL,
        'date': dateText.value,
        'desc': desctiptionController.text ?? "" ,
        'tag': tagController.text ?? "",
        'location': locationController.text
      });
      await _memoriesRepository.updateMemoryToLocal(oldValue: memory,newValue: newValue);
      MemoriesController ctrl = Get.find();
      HomeController homeController = Get.find();

    ctrl.loadMemories();
    homeController.loadMemories();
      Get.back();
      Get.back(result: false);
    }
  }

  Future<Null> _cropImage(String path) async {
    File croppedFile = await ImageCropper.cropImage(
      cropStyle: CropStyle.circle,
        sourcePath: path,
        aspectRatioPresets: Platform.isAndroid
            ? [
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio16x9
              ]
            : [
                CropAspectRatioPreset.original,
                CropAspectRatioPreset.square,
                CropAspectRatioPreset.ratio3x2,
                CropAspectRatioPreset.ratio4x3,
                CropAspectRatioPreset.ratio5x3,
                CropAspectRatioPreset.ratio5x4,
                CropAspectRatioPreset.ratio7x5,
                CropAspectRatioPreset.ratio16x9
              ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));

    if (croppedFile != null) {
      memoryImage.value = croppedFile;
    }
  }


}
