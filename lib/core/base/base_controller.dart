import 'package:get/get.dart';

abstract class BaseController extends GetxController with SingleGetTickerProviderMixin{

  void init();
  void close();

  @override
  void onInit() {
    init();
    super.onInit();
  }

  @override
  void onClose() {
    close();
    super.onClose();
  }
}