import 'package:get_storage/get_storage.dart';

abstract class BaseStorage {
  GetStorage storage;
  BaseStorage(){
    storage = GetStorage();
  }
}