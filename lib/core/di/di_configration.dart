import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/app/data/repo/memories/memories_repo.dart';
import 'package:becouple/app/presentation/screens/home/controller/home_controller.dart';
import 'package:becouple/app/presentation/screens/memories/controller/memories_controller.dart';
import 'package:becouple/app/presentation/screens/splash_page/controller/splash_controller.dart';
import 'package:get/get.dart';

class DependenciesInjection extends Bindings {

  @override
  void dependencies() {
    Get.lazyPut(() =>AuthRepository());
    Get.put(CouplesRepository());
    Get.put(MemoriesRepository());
    Get.put(SplashController());
    Get.put(MemoriesController());
    Get.lazyPut(() => HomeController());
  }
}