import 'package:becouple/app/data/repo/auth/auth_repo.dart';
import 'package:becouple/app/data/repo/couples/couples_repo.dart';
import 'package:becouple/core/base/base_module.dart';
import 'package:get/get.dart';

class RepositoriesModule extends BaseModule {
  @override
  void init() {
    Get.lazyPut(() => AuthRepository());
    Get.lazyPut(() => CouplesRepository());
    Get.put(CouplesRepository());
  }
}
