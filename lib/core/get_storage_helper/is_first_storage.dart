import 'package:becouple/core/base/base_storage.dart';

class FistStorage extends BaseStorage {

  static var shared = FistStorage();
  
  Future<void> setIsFirst() async{
    var count = storage.read<int>("isFirst") ?? 0;
    await storage.write("isFirst", count+1);
  }
  
  int getIsFirst(){
    var count = storage.read<int>("isFirst");
    return count ?? 0;
  }
}