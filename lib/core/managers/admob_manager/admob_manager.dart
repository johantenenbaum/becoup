import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:becouple/core/get_storage_helper/is_first_storage.dart';
import 'package:flutter/material.dart';

class AdmobManager {
  static var interstitalIsShow = false;
  static AdmobInterstitial interstitialAd;

  static init() {
    Admob.initialize();
    loadInterstatial();
  }

  static Widget banner() {
    if (FistStorage.shared.getIsFirst() > 1)
      return AdmobBanner(
          adUnitId: "ca-app-pub-3940256099942544/6300978111",
          adSize: AdmobBannerSize.BANNER);
    else {
      return Container();
    }
  }

  static loadInterstatial() {
    if (!interstitalIsShow) {
      interstitialAd = AdmobInterstitial(
        adUnitId: getInterstitialAdUnitId(),
      );
      interstitialAd.load();
    }
  }

  static showInterstatial(){
    interstitialAd.show();
    interstitalIsShow = true;
  }

  static String getInterstitialAdUnitId() {
    if (Platform.isIOS) {
      return 'ca-app-pub-3940256099942544/4411468910';
    } else if (Platform.isAndroid) {
      return 'ca-app-pub-3940256099942544/1033173712';
    }
    return null;
  }

  //ca-app-pub-3940256099942544/6300978111
}
