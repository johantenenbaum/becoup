import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FireBaseAuthentication {
  FirebaseAuth auth;

  FireBaseAuthentication() {
    auth = FirebaseAuth.instance;
    print(auth.currentUser);
  }

  Future<User> signInWithGoogle() async {
    try {
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
      if (googleUser == null) {
        return Future.error("Canceled SignIn");
      }
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      var userCredential =
          await FirebaseAuth.instance.signInWithCredential(credential);
      return userCredential.user;
    } on PlatformException catch (err) {
      print(err);
      return null;
    } catch (err) {
      print(err);
      return null;
    }
  }

  Future<void> signInWithFacebook() async {
    try {
      final result = await FacebookAuth.instance.login();
      final FacebookAuthCredential facebookAuthCredential =
          FacebookAuthProvider.credential(result.token);
      await FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
    } catch (e) {
      print("Hata");
    }
  }

  Stream<User> userStatus() {
    return auth.authStateChanges();
  }

  User currentUser(){
    return auth.currentUser;
  }

  Future<void> signOut()async{
    await auth.signOut();
  }

}
