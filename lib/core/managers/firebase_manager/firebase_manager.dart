
import 'package:firebase_core/firebase_core.dart';

class FireBaseManager {

  static var shared = FireBaseManager();
  
  Future<void> setup()async{
    await Firebase.initializeApp();
  }
}