
import 'package:becouple/app/data/remote/model/couples.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class FirebaseFireStore {

  FirebaseFirestore _firestore;

  FirebaseFireStore(){
    _firestore = FirebaseFirestore.instance;
  }

  Future<void> addUser({String userID,String yourName,String theirName,String date,String yourImage,String theirImage})async{
    var userRef = _firestore.collection('users');
    await userRef.doc(userID).set({
      'userID' : userID,
      'yourName' : yourName,
      'theirName' : theirName,
      'date' : date,
      'yourImage' : yourImage,
      'theirImage' : theirImage
    });
  }

  Future<bool> userIsAvailable({String userID})async{
    var ref = _firestore.collection('users');
    var snapshot = await ref.doc(userID).get();
    return snapshot.exists;
  }

  Future<Couple> fetchCouples(String userID)async{
    var document = _firestore.collection('users').doc(userID);
    var snapshot = await document.get();
    var couple = Couple.fromJson(snapshot.data());
    return couple;
  }

  Future<void> addMemory(String userID,{String uuid,String imageURL,String title,String description,String date,String location,String tag})async{
    var ref = _firestore.collection('memories').doc(userID).collection('memoriesCollection').doc(uuid);
    await ref.set({
      'id' : uuid,
      'imageURL' : imageURL,
      'title' : title,
      'desc' : description,
      'date':date,
      'location' : location,
      'tag':tag
    });
  }

  Future<List<Map<String, dynamic>>> fetchMemories(String userID)async{
    var collectionRef = _firestore.collection('memories').doc(userID).collection('memoriesCollection');
    var snapshot = await collectionRef.get();
    List<Map<String, dynamic>> dataList = [];

    for(var item in snapshot.docs){
      var data = await item.reference.get();
      dataList.add(data.data());
    }

    return dataList;
  }

  Future<void> updateMemory(String userID,String documentID,Map<String,dynamic> data)async{
    var docRef = _firestore.collection('memories').doc(userID).collection('memoriesCollection').doc(documentID);
    await docRef.update(data);
  }

  Future<void> deleteMemory(String userID,String documentID)async{
    var docRef = _firestore.collection('memories').doc(userID).collection('memoriesCollection').doc(documentID);
    await docRef.delete();
  }

}