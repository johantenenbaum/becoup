import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class FirebaseStorageManager{

  firebase_storage.FirebaseStorage _storage;

  FirebaseStorageManager(){
    _storage = firebase_storage.FirebaseStorage.instance;
  }

    Future<String> uploadImage({String userID,File file,bool isYour})async{
    try {
      var task = await _storage.ref().child(userID).child(isYour ? "your.png" : 'their.png').putFile(file);
      var url = await task.ref.getDownloadURL();
      return url;
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future<String> uploadMemories({String userID,File file})async{
    try {
      var task = await _storage.ref().child(userID).child('memories').child("${DateTime.now().millisecondsSinceEpoch}.png").putFile(file);
      var url = await task.ref.getDownloadURL();
      return url;
    } catch (e) {
      print(e);
      return null;
    }
  }



}