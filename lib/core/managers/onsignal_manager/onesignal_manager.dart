import 'package:onesignal_flutter/onesignal_flutter.dart';

class OneSignalManager{
  static var shared = OneSignalManager();
  final _appID = "6c651abc-a147-4e94-96a2-1616894e0aca";

  OneSignalManager() {
    OneSignal.shared.setNotificationReceivedHandler((OSNotification notification) {
      _handleNotificationReceived(notification);
    });

    OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      _handleNotificationOpened(result);
    });
  }

  void _handleNotificationReceived(OSNotification notification) async{
  }

  void _handleNotificationOpened(OSNotificationOpenedResult result) async{
  }

  Future<void> initOneSignal() async {
    await OneSignal.shared.init(_appID);
    OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
  }

}
