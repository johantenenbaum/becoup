import 'package:flutter/services.dart';
import 'package:purchases_flutter/purchases_flutter.dart';

class PurchaseHelper {
  static var shared = PurchaseHelper();
  var isPremium = false;

  Future<void> initPurchase() async {
    await Purchases.setDebugLogsEnabled(true);
    await Purchases.setup("gJELDAwqbWYoeAGkGtsuhXanEiKUqNXd");
    await isSubscription();
  }

  Future<bool> purchase(String id) async {
    try {
      var info = await Purchases.purchaseProduct(id);
      isPremium = info.entitlements.all['Subscription'].isActive;
      return isPremium;
    } catch (e) {
      var errorCode = PurchasesErrorHelper.getErrorCode(e);
      print("Ödemede Hata Var $errorCode");
      return false;
    }
  }

  Future<void> isSubscription() async {
    var purchaserInfo = await Purchases.getPurchaserInfo();
    isPremium = purchaserInfo.entitlements.all["Subscription"] != null &&
        purchaserInfo.entitlements.all["Subscription"].isActive;
        print(isPremium);
  }

  Future<void> restorePurchase() async {
    try {
      PurchaserInfo restoredInfo = await Purchases.restoreTransactions();

      isPremium = restoredInfo.entitlements.all['subscription'].isActive;
    } on PlatformException catch (e) {
      print(e);
    }
  }
}
