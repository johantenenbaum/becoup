import 'package:url_launcher/url_launcher.dart';

class UrlLauncherManager {
  static var shared = UrlLauncherManager();

  Future<void> launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Açılamadı $url';
    }
  }

  String email(String mail, {String subject = "Subject"}) {
    return Uri(
        scheme: 'mailto',
        path: mail,
        queryParameters: {'subject': subject}).toString();
  }
}
