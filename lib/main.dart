import 'package:becouple/app/data/remote/model/memory_data.dart';
import 'package:becouple/core/di/di_configration.dart';
import 'package:becouple/core/get_storage_helper/is_first_storage.dart';
import 'package:becouple/core/managers/admob_manager/admob_manager.dart';
import 'package:becouple/core/managers/firebase_manager/firebase_manager.dart';
import 'package:becouple/core/managers/onsignal_manager/onesignal_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:hive/hive.dart';
import 'app/presentation/screens/splash_page/splash_page.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:becouple/core/managers/purchase_helper/purchase_helper.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  AdmobManager.init();

  await FireBaseManager.shared.setup();
  await OneSignalManager.shared.initOneSignal();
  await GetStorage.init();
  await Hive.initFlutter();
  await PurchaseHelper.shared.initPurchase();
  await FistStorage.shared.setIsFirst();
  Hive.registerAdapter(MemoryDataAdapter());
  await Hive.openBox<MemoryData>('memories');

  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    initialBinding: DependenciesInjection(),
    home: SplashPage(),
  ));
}